export class ConstantClass {
    static webserviceName = 'http://172.104.180.157/epadwstest/';
    static Email = 'emailid';
    static Passoword = 'password';
    static ReceptesString = "Receptinjoo"


    static Recipes = [
        {
            id: 0, 
            name: "Salats", 
            category:"Brokastis", 
            portions: 4, 
            instructions:"Sagriez un samaisi", 
            products: [
                {
                    name: "Gurkis", 
                    amount:5,
                    unit: "gab",
                },
                {
                    name: "Kartupelis", 
                    amount:3,
                    unit: "kg",
                }
            ]
        },
        {
            id: 1, 
            name: "Rasols", 
            category:"Pusdienas", 
            portions: 8, 
            instructions:"Samaisi un vāri 10 min vai kamēr nomirsti", 
            products: [
                {
                    name: "Sviests", 
                    amount:66,
                    unit: "g",
                },
                {
                    name: "Pipars", 
                    amount:233,
                    unit: "naža gals",
                }
                ]
        },
    ]

    //TODO katram sarakstam vienmer vajadzes vienu recepti by defaulty kas ir vina recepte (kura likt individualas lietas)
    static PlanList = [
        {
            id: 1, 
            name: "Saraksts #2",
            date: new Date(2020, 1, 19),
            recipes: [1,2,2]
        },

        {
            id: 2, 
            name: "Saraksts #4",
            date: new Date(2020, 1, 20),
            recipes: [2,2,2]
        },
    ]

    static getRecipe(recipeId)
    {
        if (recipeId == null) {
            recipeId = this.Recipes.length;
            this.Recipes.push(
                {
                    id: recipeId,
                    name: "", 
                    category:"", 
                    portions: 0, 
                    instructions:"", 
                    products: []
                }
            ) 
        }
        
        return this.Recipes[recipeId];
    }

    static updateRecipe(recipe)
    {
        var position = this.Recipes.findIndex(function(element, index, array){return element.id === recipe.id})
        this.Recipes[position] = recipe;
    }

    static deleteRecipe(recipeId)
    {
        this.Recipes = this.Recipes.filter(item => item.id !== recipeId)
    }

    static addToPlan(recipeId)
    {
        //TODO:: add to plan
        //this.CurrentPlan.push(this.getRecipe(recipeId));
    }

    static getPlan(id)
    {
        return this.PlanList.filter(plan => plan.id == id)[0];
    }

    static getProducts(plan)
    {
        var resData = {};
        plan.recipes.forEach(recipeId => {

                var recipeData = this.getRecipe(recipeId)

                recipeData.products.forEach(product => {
                    if (resData.hasOwnProperty(product.name)) {
                        resData[product.name].amount += product.amount;
                    } else {
                        resData[product.name] = {name: product.name, amount: product.amount};
                    }
                });
            }
        );

        //quickhack to get dictionary values
        var res = Object.keys(resData).map(function(key){
            return resData[key];
        });

        return res;
    }
}
