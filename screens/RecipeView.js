import React from 'react';
import { View, StyleSheet,Alert, Text, TouchableHighlight, TouchableOpacity } from 'react-native';

import {Button, TextInput} from "react-native-web";
import { ConstantClass } from "@constants/ConstantFile";
import { createStackNavigator } from '@react-navigation/stack'
import { useState } from 'react';
import { SwipeRow } from 'react-native-swipe-list-view';
import Colors from '../constants/Colors'
import ProductRow from '../components/product/ProductRow'
import RecipeDeleteModal from '../components/RecipeDeleteModal'

const Stack = createStackNavigator();

const RecipeView = ({ route, navigation, props }) => {

    const [recipe, setRecipe] = useState(ConstantClass.getRecipe((route.params ? route.params.recipeId : 1)));
    const [modalVisible, setModalVisible] = useState(false);

    const toggleModal = () => {
        console.log("VVV togglng modal:" + !modalVisible)
        setModalVisible(!modalVisible);
    };

    const deleteRecipe = () => {
        ConstantClass.deleteRecipe(recipe.id);
        route.params.updateHandler(ConstantClass.Recipes);
        navigation.navigate('recipe');
    };

    //trigger change to re-render on back
    const [refreshKey, setRefreshKey] = useState(0); 
    const unsubscribe = navigation.addListener('focus', () => {
        setRefreshKey(Math.random());
    });

	return (
        <View style={styles.screen}>

            <RecipeDeleteModal
                display = {modalVisible}
                toggleModal = {toggleModal}
                deleteRecipe = {deleteRecipe}
                recipeName = {recipe.name}
            />


            <SwipeRow
             rightOpenValue={-150}
             previewRowKey={'0'}
             previewOpenValue={-40}
             previewOpenDelay={3000}
            >
                <View style={styles.rowBack}>
                    <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnLeft]}
                                onPress={() => {
                                        setModalVisible(true);
                                        /*
                                        //ConstantClass.deleteRecipe(recipe.id);
                                        //route.params.updateHandler(ConstantClass.Recipes);
                                        //navigation.navigate('recipe');
                                        */
                                    }
                                }
                            > 
                            <Text>DELETE</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]}
                        onPress={() => {
                            navigation.navigate('recipe-edit',{ recipeId: recipe.id, updateHandler: route.params.updateHandler});
                        }}
                    > 
                        <Text>EDIT</Text>
                    </TouchableOpacity>
                </View>


                <View style={styles.titleBlock}>
                    <Text style={styles.recipeName}>{recipe.name}</Text>
                    <Text style={styles.recipeInfo}>{recipe.category}    {recipe.portions} porcijas</Text>
                </View>
            </SwipeRow>

            <View style={styles.recipeBlock}>
                {recipe.products.map((product) => 
                    <ProductRow key={product.name} product = {product} />
                )}
            </View>
            
            <View style={styles.instructionBlock}>
                <Text>{recipe.instructions}</Text>
            </View >
            
            <View style={styles.bottomBlock}>
                <Button style={styles.addListButton} title="ADD TO LIST" 
                    onPress={() => {
                        ConstantClass.addToPlan(recipe.id);
                        navigation.navigate('recipe');
                    }}
                />
            </View>
            

            

        </View>
	);
}

export default RecipeView;


const styles = StyleSheet.create({
	screen: {
        height: '100%',
        padding: 20,
        flexDirection: 'column',
        backgroundColor: Colors.White,
    },

    titleBlock: {
        flex: 1,
        backgroundColor: Colors.White,
        paddingBottom: 20,
    },
    recipeName: {
        fontFamily: "Franklin Gothic",
        fontSize: 37,
    },
    recipeInfo: {
        fontFamily: "Franklin Gothic",
        fontSize: 16,
        color: '#8F8282',
    },

    //=======Under the sliding element

    rowBack: {
        flex: 1,
        alignItems: 'center',
        //backgroundColor: '#DDD',
        
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },

    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnLeft: {
        backgroundColor: 'blue',
        right: 75,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0,
    },

    backTextWhite: {
        color: '#FFF',
    },


//--------------------------------
    
    recipeBlock: {
        //flex:1
        flexDirection: 'column',
        //backgroundColor: '#6ED4C8',
        paddingBottom: 40,
    },

//-----------------------------------

    instructionBlock: {
        fontFamily: "Franklin Gothic",
        fontSize: 14,
    },


    bottomBlock: {
        position: 'absolute',
        bottom: 10,
        flex: 1,
    },
    

    addListButton: {
        width:100,
        height: 100,
    },


   


    
    

})