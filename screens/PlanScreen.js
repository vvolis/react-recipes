import React from 'react';
import { View, StyleSheet, Text, Modal, TouchableHighlight, TouchableOpacity } from 'react-native';
import {Button, TextInput} from "react-native-web";
import { ConstantClass } from "@constants/ConstantFile";
import { createStackNavigator } from '@react-navigation/stack'
import { useState } from 'react';
import { SwipeListView } from 'react-native-swipe-list-view';
import Colors from '../constants/Colors'


const PlanScreen = ({ route, navigation, props }) => {

    const [plans, setPlans] = useState(ConstantClass.PlanList);


    const closeRow = (rowMap, rowKey) => {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    };

    const deleteRow = (rowMap, rowKey) => {
        /*closeRow(rowMap, rowKey);
        const newData = [...listData];
        const prevIndex = listData.findIndex(item => item.key === rowKey);
        newData.splice(prevIndex, 1);
        setListData(newData);*/
    };

    const onRowDidOpen = rowKey => {
        //console.log('This row opened', rowKey);
    };

    function formatDate(date) {
        var month = '' + (date.getMonth() + 1),
            day = '' + date.getDate(),
            year = date.getFullYear();

        return [day, month, year].join('/');
    }

    const renderItem = data => (
        <View style={styles.planListItem} key={data.item.id} underlayColor={'#AAA'}>
            <Text style={styles.planListItemName} onPress={() =>
                    navigation.navigate('plan-view', { planId: data.item.id })
                }
            >
                {data.item.name}
            </Text>
            <Text style={styles.planListItemDate}>{formatDate(data.item.date)}</Text>
        </View>
    );

    const renderHiddenItem = (data, rowMap) => (
        <View style={styles.rowBack}>
            <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnLeft]}
                onPress={() => closeRow(rowMap, data.item.key)}
            >
                <Text style={styles.backTextWhite}>Close</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnRight]}
                onPress={() => deleteRow(rowMap, data.item.key)}
            >
                <Text style={styles.backTextWhite}>Delete</Text>
            </TouchableOpacity>
        </View>
    );

    return (
        <View style={styles.screen}>
            <View style={styles.titleHolder}> 
                <Text style={styles.title}>Iepirkumu saraksti</Text>
            </View>
            
            <SwipeListView styles={{flex:1}}
                data={plans}
                renderItem={renderItem}
                renderHiddenItem={renderHiddenItem}
                rightOpenValue={-150}
                previewRowKey={'0'}
                previewOpenValue={-40}
                previewOpenDelay={3000}
                onRowDidOpen={onRowDidOpen}
            />
        </View>
    );
}

export default PlanScreen;


const styles = StyleSheet.create({
	screen: {
        backgroundColor: Colors.White,
		flex: 1,
    },
    titleHolder: {
        alignItems: 'center', //horizontal alignment
    },
    title: {
        
        paddingTop: 20,
        paddingBottom: 20,
        fontFamily: "Franklin Gothic",
        fontSize: 37,
    },

    planListItem:{
        width: '100%',
        paddingHorizontal: 40,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center', //Vertical centering
        justifyContent: 'space-between',
        flex: 1,
        backgroundColor: Colors.White,
    },
    planListItemName: {
        fontFamily: "Franklin Gothic",
        fontSize: 22,
    },
    planListItemDate:{
        fontFamily: "Franklin Gothic",
        fontSize: 16,
    },


    //=======Under the sliding element

    rowBack: {
        alignItems: 'center',
        backgroundColor: '#DDD',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },

    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnLeft: {
        backgroundColor: 'blue',
        right: 75,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0,
    },

    backTextWhite: {
        color: '#FFF',
    },
})