import React from 'react';
import { View, StyleSheet, Text, Modal, TouchableHighlight } from 'react-native';
import {Button, TextInput} from "react-native-web";
import { ConstantClass } from "@constants/ConstantFile";
import { createStackNavigator } from '@react-navigation/stack'
import { useState } from 'react';

const Stack = createStackNavigator();

const RecipeScreen = ({ route, navigation, props }) => {

	const [recipeList, setRecipeList] = useState(ConstantClass.Recipes);
	//trigger change to re-render on back
    const [refreshKey, setRefreshKey] = useState(0); 
    const unsubscribe = navigation.addListener('focus', () => {
        setRefreshKey(Math.random());
    });

	return (
	<View style={styles.screen}>
		<Text key={refreshKey}>CategoryName</Text>
		<View>
			<View styles={styles.recipeBlock}>
				{recipeList.map((recipe) => 
					<Text 
						key={recipe.id} 
						onPress={() =>
							navigation.navigate('recipe-view', { recipeId: recipe.id, updateHandler: setRecipeList })
						}
					>
						{recipe.name}
					</Text>
				)}
			</View>
		</View>

		<Button
			title="ADD"
			onPress={() =>
                navigation.navigate('recipe-edit', { recipeId: null, updateHandler: setRecipeList })
			}
		/>
	</View>

	
	);
}

export default RecipeScreen;


const styles = StyleSheet.create({
	screen: {
		flex: 1,
		padding: 10,
		alignItems: 'center', //horizontal alignment
	},

	recipeBlock: {
        flex: 1
    }
    
})