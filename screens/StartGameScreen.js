import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import {Button, TextInput} from "react-native-web";

const StartGameScreen = props => {
	return (
		<View style={styles.screen}>
			<Text style={styles.title}>Start a New Game!</Text>
			<View style={styles.inputContainer} >
				<Text>Select a Number!</Text>
				<TextInput />
				<View style={styles.buttonContainer}>
					<Button title="Reset" onPress={()=>{}} />
					<Button title="Confirm" onPress={()=>{}} />
				</View>
			</View>
		</View>
	);
}

export default StartGameScreen;

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		padding: 10,
		alignItems: 'center', //horizontal alignment
	},
	buttonContainer: {
		flexDirection: 'row',
		width: '100%',
		justifyContent: 'space-between',
		paddingHorizontal: 15
	},
	inputContainer: {
		width: 300,
		maxWidth: '80%',
		alignItems: 'center',
		backgroundColor: 'white',
		padding: 20,
		borderRadius: 10,
		//iOS only
		shadowColor: 'black',
		shadowOffset: { width: 0, height: 2},
		shadowRadius: 6,
		shadowOpacity: 0.26,
		//Android shadow
		elevation: 5,

	},
	title: {
		fontSize: 20,
		marginVertical:10, //margin top+bot
	}
})