import React from 'react';
import { View, StyleSheet, Text, Modal, TouchableHighlight } from 'react-native';
import {Button, TextInput} from "react-native-web";
import { ConstantClass } from "@constants/ConstantFile";
import { createStackNavigator } from '@react-navigation/stack'
import { useState } from 'react';
import ProductInput from '../components/product/ProductInput'

const Stack = createStackNavigator();



const RecipeEdit = ({ route, navigation, props }) => {

    const [recipe, setRecipe] = useState(
            ConstantClass.getRecipe(route.params ? route.params.recipeId : null)
        );
    
    function _onEditProduct(index, name)
    {
        console.log(index, recipe.products.length);
        if (recipe.products.length <= index) {
            console.log("push");
            recipe.products.push({});
        }
        console.log(recipe.products);
        recipe.products[index].name = name;
        setRecipe(recipe);
    }

    function _onEditProductAmount(index, amount)
    {
        recipe.products[index].amount = amount;
        setRecipe(recipe)
    }

    function _onEditProductUnit(index, unit)
    {
        recipe.products[index].unit = unit;
        setRecipe(recipe)
    }

    function _onEditRecipeName(text)
    {
        recipe.name = text;
        setRecipe(recipe)
    }

    function _onEditRecipeCategory(category)
    {
        recipe.category = category;
        setRecipe(recipe)
    }

    function _onEditRecipePortions(portions)
    {
        recipe.portions = portions;
        setRecipe(recipe)
    }

    function _onEditRecipeInstructions(instructions)
    {
        recipe.instructions = instructions;
        setRecipe(recipe)
    }


	return (
        <View style={styles.screen}>
           
            <View> 
                <Text style={styles.inputLabel} >Nosaukums</Text>
                <TextInput style={styles.recipeNameInput} defaultValue={recipe.name} placeholder="Receptes nosaukums"
                    onChangeText={
                        (text) => { _onEditRecipeName(text); }
                    }
                />
            </View>

            <View style={styles.itemList}> 
                <Text style={styles.inputLabel} >Sastāvdaļas</Text>

                {recipe.products.map((product, index) => 
                    <ProductInput key={product.name}
                        product={product} 
                        productIndex = {index}
                        onEditName = {_onEditProduct}
                        onEditAmount = {_onEditProductAmount}
                        onEditUnit = {_onEditProductUnit}
                    />
                )}

                <ProductInput key={recipe.products.length}
                    productIndex = {recipe.products.length}
                    onEditName = {_onEditProduct}
                    onEditAmount = {_onEditProductAmount}
                    onEditUnit = {_onEditProductUnit}
                />
            </View>

            <View style={styles.categoryPortionBlock}>
                <View>
                    <Text style={styles.inputLabel} >Kategorija</Text>
                    <TextInput style={[styles.inputBox]} defaultValue={recipe.category} onChangeText={(text) => { _onEditRecipeCategory(text); }}/>
                </View>
                <View>
                    <Text style={styles.inputLabel} >Porcijas</Text>
                    <TextInput style={[styles.inputBox]} defaultValue={recipe.portions} onChangeText={(text) => { _onEditRecipePortions(text); }}/>
                </View>

            </View>

            <View>
                <Text style={styles.inputLabel} >Instrukcijas</Text>
                <TextInput style={[styles.inputBox]} defaultValue={recipe.instructions} onChangeText={(text) => { _onEditRecipeInstructions(text); }}/>
            </View>

            

            


            <Button style={styles.button} title="SAVE" onPress={() => {
                        ConstantClass.updateRecipe(recipe);
                        route.params.updateHandler(ConstantClass.Recipes);
                        navigation.goBack(null);
                    }
                }/>

           

        </View>
    );
    
    
}

export default RecipeEdit;


const styles = StyleSheet.create({
	screen: {
		flex: 1,
		padding: 40,
		//alignItems: 'center', //horizontal alignment
        flexDirection: 'column',
    },

    button: {
        width:100,
        height: 100,
        flex:0.5
    },


    itemList: {
        flexDirection: 'column',
        //backgroundColor: 'yellow'
    },

    recipeNameInput: {
        borderColor: 'black',
        borderWidth: 1,
        fontFamily: "Franklin Gothic",
        fontSize: 37,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 5,
    },
    
    inputLabel: {
        paddingBottom: 10,
        fontFamily: "Franklin Gothic",
        fontSize: 16,
    },

    inputBox: {
        borderColor: 'black',
        borderWidth: 1,
        fontFamily: "Franklin Gothic",
        paddingHorizontal: 5,
        paddingVertical: 10,
        //marginHorizontal: 5
    },

    categoryPortionBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    
})