import React from 'react';
import { View, StyleSheet, Text, Modal, TouchableHighlight, TouchableOpacity } from 'react-native';
import {Button, TextInput} from "react-native-web";
import { ConstantClass } from "@constants/ConstantFile";
import { useState } from 'react';
import { SwipeListView } from 'react-native-swipe-list-view';
import Colors from '../constants/Colors'

function formatDate(date) {
    var month = '' + (date.getMonth() + 1),
        day = '' + date.getDate(),
        year = date.getFullYear();

    return [day, month, year].join('/');
}

const PlanView = ({ route, navigation, props }) => {

    const [plan, setPlan] = useState(ConstantClass.getPlan(route.params ? route.params.planId : 1));

    const closeRow = (rowMap, rowKey) => {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    };

    const deleteRow = (rowMap, rowKey) => {
        /*closeRow(rowMap, rowKey);
        const newData = [...listData];
        const prevIndex = listData.findIndex(item => item.key === rowKey);
        newData.splice(prevIndex, 1);
        setListData(newData);*/
    };

    const onRowDidOpen = rowKey => {
        //console.log('This row opened', rowKey);
    };

    function formatDate(date) {
        var month = '' + (date.getMonth() + 1),
            day = '' + date.getDate(),
            year = date.getFullYear();

        return [day, month, year].join('/');
    }

    const renderItem = data => (
        <View style={styles.planListItem}> 
            <Text style={styles.planListItemName}>{data.item.name}</Text>
            <Text style={styles.planListItemAmount}>{data.item.amount}</Text>
        </View>
    );

    const renderHiddenItem = (data, rowMap) => (
        <View style={styles.rowBack}>
            <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnLeft]}
                onPress={() => closeRow(rowMap, data.item.key)}
            >
                <Text style={styles.backTextWhite}>Close</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnRight]}
                onPress={() => deleteRow(rowMap, data.item.key)}
            >
                <Text style={styles.backTextWhite}>Delete</Text>
            </TouchableOpacity>
        </View>
    );

    return (
            <View style={styles.screen}>
                <View style={styles.titleHolder}> 
                    <Text style={styles.planName}>{plan.name}</Text>
                    <Text style={styles.planDate}>{formatDate(plan.date)}</Text>
                </View>

                <View style={styles.recipeList}>
                    {plan.recipes.map((recipeId, index) => 
                        <View key={recipeId} style={styles.recipeItem}>
                            <Text>{ConstantClass.getRecipe(recipeId).name}</Text>
                        </View>
                    )}
                </View>
                
                <SwipeListView styles={{flex:1}}
                    data={ConstantClass.getProducts(plan)}
                    renderItem={renderItem}
                    renderHiddenItem={renderHiddenItem}
                    rightOpenValue={-150}
                    previewRowKey={'0'}
                    previewOpenValue={-40}
                    previewOpenDelay={3000}
                    onRowDidOpen={onRowDidOpen}
                />
            </View>
    )
;}

export default PlanView;


const styles = StyleSheet.create({
	screen: {
        backgroundColor: Colors.White,
		flex: 1,
    },
    titleHolder: {
        paddingTop: 20,
        paddingBottom: 20,
        alignItems: 'center', //horizontal alignment
    },
    planName: {
        fontFamily: "Franklin Gothic",
        fontSize: 37,
    },
    planDate: {
        fontFamily: "Franklin Gothic",
        fontSize: 23,
    },


    recipeList: {
        flexDirection: 'row',
        alignItems: 'center', //Vertical centering,
        justifyContent: 'center',
        paddingBottom: 20,
    },
    recipeItem: {
        paddingHorizontal: 10,
        marginHorizontal: 5,
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 50,
        fontSize: 16,
        fontFamily: "Franklin Gothic",
    },



    planListItem:{
        width: '100%',
        paddingHorizontal: 40,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center', //Vertical centering
        justifyContent: 'space-between',
        flex: 1,
        backgroundColor: Colors.White,
    },
    planListItemName: {
        fontFamily: "Franklin Gothic",
        fontSize: 22,
    },
    planListItemAmount:{
        fontFamily: "Franklin Gothic",
        fontSize: 16,
    },


    //=======Under the sliding element

    rowBack: {
        alignItems: 'center',
        backgroundColor: '#DDD',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },

    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnLeft: {
        backgroundColor: 'blue',
        right: 75,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0,
    },

    backTextWhite: {
        color: '#FFF',
    },
})