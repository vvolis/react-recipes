import React from 'react';
import { View, StyleSheet, TouchableHighlight, Text } from 'react-native';
import {Button,} from "react-native-web";
import { NavigationContainer, } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import RecipeScreen from './RecipeScreen'
import RecipeView from './RecipeView'
import RecipeEdit from './RecipeEdit'
import PlanScreen from './PlanScreen'
import PlanView from './PlanView'
import Colors from '../constants/Colors'

const Stack = createStackNavigator();


//TODO::Next

//Automatiski tukss produkts lai paradas kad sak ieprieksejo pildit recepte
//Garaks recipe edit instruction bloks
//Delete product no recipeedit

//Vienotu stick to bottom tam pogam elementu
//Pievienot sarakstam produktu vai recepti no saraksta lapas

const MainScreen = props => {
	return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="recipe-edit" TODO="THIS IS THE ENTRY POINT"> 

                <Stack.Screen name="home" component={HomeButtons} />

                <Stack.Screen name="plan" component={PlanScreen} />
                <Stack.Screen name="plan-view" component={PlanView} />
                <Stack.Screen name="recipe" component={RecipeScreen} />
                <Stack.Screen name="recipe-view" component={RecipeView} />
                <Stack.Screen name="recipe-edit" component={RecipeEdit} />
                
            </Stack.Navigator>
        </NavigationContainer>
	);
}

export default MainScreen;

const HomeButtons = ({ navigation }) => {
    return (
        <View style={styles.screen}>


            <View style={styles.buttonContainer}>
                <TouchableHighlight style={styles.buttonWrapper} onPress={() => navigation.navigate('plan')} >
                    <View style={styles.button}>
                        <Text style={styles.buttonText} > 
                        Iepirkumu saraksti
                        </Text>
                    </View>
                </TouchableHighlight>

                <TouchableHighlight style={styles.buttonWrapper} onPress={() => navigation.navigate('recipe')} >
                    <View style={styles.button}>
                        <Text style={styles.buttonText} > 
                            Receptes
                        </Text>
                    </View>
                    
                </TouchableHighlight>
            </View>
        
        
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
    },

    buttonContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        position: 'absolute', 
        bottom: 0,
        paddingBottom: 20,
        width: '80%',
    },

    buttonWrapper: {
        borderRadius: 50,
        width: '100%',
        height: 72,
        backgroundColor: Colors.Green,
        alignItems: 'center',
        paddingHorizontal: 15,
        marginVertical: 10
    },

    button: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },

    buttonText: {
        fontSize: 22,
        fontFamily: "Franklin Gothic",
        textAlign: 'center',
    },

    
});