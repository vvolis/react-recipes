import React from 'react';
import {StyleSheet, View, TouchableOpacity, Text} from 'react-native';
import {TextInput} from "react-native-web";
import Colors from '../../constants/Colors'
import { SwipeRow } from 'react-native-swipe-list-view';

const ProductInput = props => {
    return <SwipeRow
            rightOpenValue={-75}
            previewRowKey={'0'}
            previewOpenValue={-40}
            previewOpenDelay={3000}
            >
                <View style={styles.rowBack}>
                    <TouchableOpacity style={[styles.backBtn]} onPress={() => {}}> 
                        <Text>Delete</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.planListItem}>
                    <TextInput placeholder="nosaukums" defaultValue={ props.product ? props.product.name : ""}  
                        style={[styles.planListItemName, styles.inputBox]} 
                        onChangeText={(text) => { props.onEditName(props.productIndex, text); }}/>

                    <View style={styles.unitInfo}>
                        <TextInput placeholder="0" defaultValue={props.product ? props.product.amount : ""} 
                        style={[styles.planListItemAmount, styles.inputBox]} onChangeText={(text) => { props.onEditAmount(props.productIndex, text); }} />
                        <TextInput placeholder="gab" defaultValue={props.product ? props.product.unit : ""} 
                        style={[styles.planListItemUnit, styles.inputBox]} onChangeText={(text) => { props.onEditUnit(props.productIndex, text); }} />
                    </View>
                </View>
            </SwipeRow>
};

function Greeting(props) {
	const isLoggedIn = props.isLoggedIn;
	if (isLoggedIn) {
	  return <UserGreeting />;
	}
	return <GuestGreeting />;
  }

export default ProductInput;

const styles = StyleSheet.create({
	planListItem:{
        width: '100%',
        
        flexDirection: 'row',
        alignItems: 'center', //Vertical centering
        justifyContent: 'space-between',
        flex: 1,
        backgroundColor: Colors.White,
    },
    planListItemName: {
        flex: 2,
        fontSize: 16,
    },

    inputBox: {
        borderColor: 'black',
        borderWidth: 1,
        fontFamily: "Franklin Gothic",
        paddingHorizontal: 5,
        paddingVertical: 10,
        //marginHorizontal: 5
    },


    unitInfo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center', //Vertical centering
        justifyContent: 'center',
    },



    planListItemAmount:{
        flex: 1,
        flexDirection: 'row',
        fontSize: 16,
        width: '10%',
        
    },
    planListItemUnit:{
        flex: 1,
        flexDirection: 'row',
        fontSize: 16,
        width: '10%',
    },

    //=======Under the sliding element

    rowBack: {
        flex: 1,
        alignItems: 'center',
        //backgroundColor: '#DDD',
        
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },

    backBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
        backgroundColor: 'red',
        right: 0,
    },


    backTextWhite: {
        color: '#FFF',
    },
});
