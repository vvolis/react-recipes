import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from '../../constants/Colors'

const ProductRow = props => {

    return <View style={styles.planListItem}>
            <Text style={styles.planListItemName} > {props.product.name} </Text>

            <View style={styles.unitInfo}>
                <Text style={styles.planListItemAmount} > {props.product.amount}  </Text>
                <Text style={styles.planListItemUnit} > {props.product.unit}   </Text>
            </View>
        </View>
	
};


export default ProductRow;

const styles = StyleSheet.create({
	planListItem:{
        width: '100%',
        //paddingHorizontal: 40,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center', //Vertical centering
        justifyContent: 'space-between',
        flex: 1,
        backgroundColor: Colors.White,
    },
    planListItemName: {
        fontFamily: "Franklin Gothic",
        fontSize: 16,
    },

    unitInfo: {
        flexDirection: 'row',
    },



    planListItemAmount:{
        fontFamily: "Franklin Gothic",
        fontSize: 16,
    },
    planListItemUnit:{
        fontFamily: "Franklin Gothic",
        fontSize: 16,
    },


});
