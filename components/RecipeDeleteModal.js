import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import Modal from 'modal-react-native-web';

const RecipeDeleteModal = props => {
	return (
		<Modal style={styles.modalView}
            visible={props.display}
            
            animationType="slide"
            onRequestClose={props.toggleModal} //for android hardware back
        >
            <View style={styles.centeredView}>
                <Text>Vai izdzēst receoti "{props.recipeName}"?</Text>
                <Button 
                    title="Dzest"
                    onPress={props.deleteRecipe}
                />
                <Button 
                    title="Atcelt"
                    onPress={props.toggleModal}
                />
            </View>
        </Modal>
	);
}

export default RecipeDeleteModal;



const styles = StyleSheet.create({
    modalView: {
        borderWidth:0,
        borderColor:'none',
        margin: 20,
        //backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
});
