import React from 'react';
import {StyleSheet, View} from 'react-native';

//...js property that joins maps I think
const Card = props => {
	return <View style={...styles.card}>{props.children}</View>
};

export default Card;

const styles = StyleSheet.create({
	card: {
		backgroundColor: 'white',
		padding: 20,
		borderRadius: 10,
		//iOS only
		shadowColor: 'black',
		shadowOffset: { width: 0, height: 2},
		shadowRadius: 6,
		shadowOpacity: 0.26,
		//Android shadow
		elevation: 5,
	}
});