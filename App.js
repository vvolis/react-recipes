import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import Header from './components/Header';
import StartGameScreen from './screens/StartGameScreen'
import MainScreen from './screens/MainScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'


const Stack = createStackNavigator();

export default function App() {
    return (
        <MainScreen/>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1 //Will ocupy entire screen as theres noone else
    }
});
